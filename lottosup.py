"""Parcoursup est une machine à frustrations [1].

Les règles de classement des candidats par chaque formation sont
opaques. Dans les formations sélectives, l'écart de notation entre les
dossiers des admis et de ceux non admis peut être extrêmement faible. Du
fait que les bulletins de notes ont un poids non négligeable, on peut se
poser la question de l'équité.

À cela, il faut ajouter l'angoisse de devoir faire des choix dans un
temps limité avant de perdre l'accès à des formations où le candidat est
accepté.

Tout cela fait que Parcoursup est un Lotto géant où ceux qui sont mal
conseillés risquent de perdre.

Ce petit programme permet de jouer à Parcoursup pour apprendre à gérer
le stress auquel élèves et parents serons soumis.


[1] https://www.lemonde.fr/idees/article/2021/05/22/parcoursup-a-echoue-a-rendre-acceptable-la-penurie-de-places-dans-l-enseignement-superieur_6081119_3232.html


Ce code est distribué sous licence publique générale GNU Affero
https://www.gnu.org/licenses/agpl-3.0.fr.html

"""

import math
import random
import sys
from enum import Enum, auto

# La phase principale de Parcoursup dure environ 1 mois et demi
DUREE_PS = 45

# Votre rang dans votre classe
RANG_CLASSE = 15

# Nombre d'élèves dans votre classe
TAILLE_CLASSE = 35

# Un voeux est un tuple ("nom", nb_places, rang_dernier_2021, taux_acceptation)
VOEUX = [
    (
        "MIT - Aerospace",
        60,
        296,
        0.1,
    ),
    (
        "Élite - Youtubeur ",
        25,
        79,
        0.1,
    ),
    (
        "Oxford - Maths",
        25,
        41,
        0.1,
    ),
    (
        "Stanford - Artificial Intelligence",
        42,
        322,
        0.1,
    ),
    (
        "HEC pour fils à Papa",
        20,
        58,
        0.1,
    ),
    ("Fac de truc qui sert à rien", 1500, 145300, 0.9999),
    ("Éleveur de chèvres dans les Pyrénées", 180, 652, 0.1),
]


class Status(Enum):
    """ Les réponses des formations """

    ACCEPT = "🎉"  # la formation a accepté
    WAIT = "⏳"
    REJECT = "☠️"  # la formation a rejeté
    DROP = "💪"  # le candidat a rejeté
    EXPIRED = "⌛"  # date limite dépassée
    CHECK = "☑️"  # le candidat a accepté définitvement


class Choix(Enum):
    """ Les choix du candidat pour chaque formation proposée """

    WAIT = auto()  # le candidat maintient l'attente
    DROP = auto()  # le candidat a rejeté
    CHECK = auto()  # le candidat a accepté définitvement
    ACCEPT = auto()  # acceptation non définitive


class Formation:
    """Modèle pour une formation.

    Le candidat a un rang dans la formation et les acceptations se font
    en fonction du nombre de places disponibles et le rang du candidat.

    On gère une liste d'attente qui s'actualise chaque jour en fonction
    des refus des autres candidats.

    """

    def __init__(self, idx, nom, places, rang_dernier_2021, taux_accept):
        """Constructeur pour la formation.

        idx : un indice unique pour chaque formation qui nous permettra
        de gérer les réponses de l'utilisateur.

        nom : le nom de la formation (pour l'affichage)

        places : nombre de places disponibles

        rang_dernier_2021 : rang du dernier candidat admis l'année
        précédente

        taux_accept : le taux d'acceptation de la formation

        """
        self.nom = nom
        self.idx = idx
        self.total_places = places
        self.rang_dernier_2021 = rang_dernier_2021
        self.rang_dernier = self.total_places
        self.taux_acceptation = taux_accept
        self.votre_rang = self.compute_rang()
        self.date_limite = DUREE_PS  # jour limite pour répondre
        self.places_disponibles = self.total_places
        self.status = None  # statut du candidat dans la formation

    def __repr__(self):
        """ Pour un affichage avec print()"""
        return (
            f"{self.idx}) {self.nom}\n"
            f"\t{self.total_places} places -"
            f" rang 2021 : {self.rang_dernier_2021}\n"
            f"\tvotre rang : {self.votre_rang} -"
            f" rang dernier : {self.rang_dernier}\n"
            f"\tdate limite : jour {self.date_limite}\n"
            f"\tStatut : {self.status} {self.status.value}\n"
        )

    def compute_rang(self):
        """Calcul du rang (classement) du candidat dans la formation

        Avec le nombre de places et le taux d'acceptation, on calcule le
        nombre de candidats. Le rang du candidat parmi tous les autres
        est calculé à partir du rang dans sa classe par une simple règle
        de trois. Pour donner un peu de réalisme, on fait un tirage
        aléatoire avec une gaussienne de moyenne égale à ce rang et un
        écart type de 10 (parce que!). La gaussienne étant à support non
        borné, on coupe entre 0 et le nombre de candidats.

        """
        nombre_candidats = self.total_places / self.taux_acceptation
        rang_moyen = RANG_CLASSE / TAILLE_CLASSE * nombre_candidats
        tirage = random.gauss(rang_moyen, 10)
        rang = int(min(nombre_candidats, max(tirage, 0)))
        return rang

    def update_date_limite(self, jour):
        """ Mise à jour de la date limite de réponse pour la formation """
        if jour < 5:
            self.date_limite = 5
        else:
            self.date_limite = jour + 1

    def update_rang_dernier(self, jour):
        """Mise à jour de la liste d'attente

        Pour simuler l'évolution de la liste d'attente, on calcule
        simplement le rang du dernier accepté dans la formation. Cela
        nous évite de devoir simuler les refus de candidats (c'est des
        adolescents, ils sont imprévisibles).

        La position du dernier admis avance avec une probabilité de loi
        exponentielle (il est plus probable que la position du dernier
        avance de peu de places que de beaucoup) p(x) = \lambda
        \exp^{-\lambda \times x}. La moyenne de cette loi (\lambda),
        décroît avec les jours qui passent (plus de candidats se
        désistent les premiers jours que par la suite.).

        Pour fixer \lambda, on part du 99è quantile (la valeur de la
        variable pour laquelle on a 99% de chances d'être en dessous).
        Pour la distribution exponentielle, la fonction quantile est q =
        -\ln(1-p)/\lambda.

        De façon arbitraire, on prend q égal à un dixième de la longueur
        de la liste d'attente.

        """
        longueur_attente = max(
            self.rang_dernier_2021 - self.rang_dernier + 1, 1
        )
        jours_restants = DUREE_PS - jour + 1
        q = longueur_attente / 10
        p = min(
            0.999, float(jours_restants / DUREE_PS) * 0.99
        )  # on fixe le max à 0.999 pour éviter des exceptions dans le log
        lam = max(-(math.log(1 - p) / q), 1e-15)
        tirage = random.expovariate(lam)
        self.rang_dernier += int(tirage)

    def update_status(self, jour, choix: Choix = None):
        """Mise à jour du statut de la formation

        Cette méthode est utilisée dans 2 cas : la mise à jour
        automatique du système chaque nuit et aussi la mise à jour après
        les choix du candidat.

        Mise à jour du lotto Parcoursup :

        Un candidat est accepté par une formation si son rang est
        inférieur ou égal au nombre de places de la formation, puis, si
        son rang est inférieur au dernier admis cette année.

        Un candidat est refusé par une formation si son rang es de 20%
        supérieur à celui du dernier admis l'année dernière (il faut
        être sévère avec ces jeunes …)

        On gère aussi l'expiration des délais d'attente de réponse.

        Gestion des choix du candidat : on se limite à mettre à jour
        avec sa réponse.

        """
        if choix is None:
            self.update_rang_dernier(jour)
            if self.votre_rang <= self.rang_dernier and (
                self.status == Status.WAIT or self.status == Status.ACCEPT
            ):
                if self.status != Status.ACCEPT:
                    self.update_date_limite(jour)
                    self.status = Status.ACCEPT
            elif self.votre_rang >= self.rang_dernier_2021 * 1.2:
                self.status = Status.REJECT
            else:
                self.status = Status.WAIT
            if self.status == Status.ACCEPT and jour > self.date_limite:
                self.status = Status.EXPIRED
        elif choix == Choix.CHECK:
            self.status = Status.CHECK
        elif choix == Choix.DROP:
            self.status = Status.DROP
        elif choix == Choix.WAIT:
            self.status = Status.WAIT


class Parcoursup:
    """ Modèle pour la simulation du processus """

    def __init__(self, voeux):
        """ On crée la classe avec les voeux et on fait un premier run """
        self.voeux = voeux
        self.jour = 0
        for v in self.voeux:
            v.update_status(self.jour)

    def iteration(self):
        """Itération journalière du système

        On commence par mettre à jour en chaque formation pour le jour courant.
        On affiche les résultats de l'algo PS®.
        On demande au candidat de faire ses choix.
        On nettoie (élimination des formations refusées par le candidat).
        """

        self.jour += 1
        for v in self.voeux:
            v.update_status(self.jour)
        self.print()
        self.choice()
        self.clean()

    def get_voeu(self, idx):
        """ Récupération d'un voeu à partir de son identifiant unique """
        for v in self.voeux:
            if v.idx == idx:
                return v

    def drop_all_accept_except(self, accept_tmp):
        """Si le candidat accepte temporairement une formation, il
        refuse automatiquement toutes les autres où il a été accepté.
        That's life. Bienvenue au monde des adultes …

        """
        for v in self.voeux:
            if v.idx != accept_tmp and v.status == Status.ACCEPT:
                v.update_status(self.jour, Choix.DROP)

    def get_input(self, message, single_value=False):
        """ Utilité pour récupérer les réponses du candidat à une question """
        print(message, end=" ")
        resp = input()
        if single_value:
            try:
                val = int(resp)
                return val
            except ValueError:
                self.get_input(message, True)
        else:
            return resp.split()

    def choice(self):
        """Gestion des choix par le candidat

        On commence par lui demander s'il veut accepter définitivement
        une formation, on passe ensuite à l'acceptation temporaire et on
        finit par récupérer les formations refusées.

        """
        accept_def = self.get_input(
            "Acceptation définitive [idx ou 0] :", True
        )
        while accept_def != 0:
            v_accept = self.get_voeu(accept_def)
            if v_accept.status == Status.ACCEPT:
                v_accept.update_status(self.jour, Choix.CHECK)
                print(
                    "Félicitations, vous avez accepté définitivement"
                    " la formation\n"
                )
                print(v_accept.nom)
                sys.exit(0)
            else:
                print(
                    "Vous n'avez pas été accepté dans la formation\n"
                    f"{v_accept.nom}\n"
                )
                accept_def = self.get_input(
                    "Acceptation définitive [idx ou 0] :", True
                )

        accept_tmp = self.get_input(
            "Acceptation temporaire [idx ou 0] :", True
        )
        while accept_tmp != 0:
            v_accept = self.get_voeu(accept_tmp)
            if v_accept.status == Status.ACCEPT:
                self.drop_all_accept_except(accept_tmp)
                v_accept.date_limite = DUREE_PS
                break
            else:
                print(
                    "Vous n'avez pas été accepté dans la formation\n"
                    f"{v_accept.nom}\n"
                )
                accept_tmp = self.get_input(
                    "Acceptation temporaire [idx ou 0] :", True
                )
        rejets = self.get_input("Rejets [id1 id2 ... ou 0] :", False)
        if rejets[0] == "0":
            return None
        for rejet in rejets:
            v_rejet = self.get_voeu(int(rejet))
            if v_rejet is not None:
                v_rejet.update_status(self.jour, Choix.DROP)

    def clean(self):
        """Élimination des formations qui ne sont ni acceptées ni en
        liste d'attente."""
        self.voeux = {
            v
            for v in self.voeux
            if (v.status == Status.ACCEPT or v.status == Status.WAIT)
        }

    def print(self):
        """Affichage des résultats du Lotto Parcoursup

        Seulement les formations où le candidat est accepté ou en liste
        d'attente sont affichées. Il ne faut pas remuer le couteau dans
        la plaie.

        """
        print(f"################# Jour {self.jour} #####################\n")
        for s in [Status.ACCEPT, Status.WAIT]:
            print(f"\t======== {s} ========")
            for v in self.voeux:
                if v.status == s:
                    print(v)


def main():
    """On lit les veux du candidat et on initialise la simulation. On
    simule (DUREE_PS = 45 par défaut) jours d'agonie et de stress."""
    voeux = {
        Formation(idx, n, p, r, tr)
        for idx, (n, p, r, tr) in enumerate(VOEUX, start=1)
    }
    ps = Parcoursup(voeux)
    print("Bienvenue dans LottoSup™.\n" "Appuyez sur une touche pour jouer!\n")
    input()
    for it in range(DUREE_PS):
        ps.iteration()


if __name__ == "__main__":
    main()
